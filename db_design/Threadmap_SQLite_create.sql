-- Created by Vertabelo (http://vertabelo.com)
-- Script type: create
-- Scope: [tables, references, sequences, views, procedures]
-- Generated at Sun Nov 23 20:14:40 UTC 2014


PRAGMA journal_mode=WAL;
PRAGMA foreign_keys = ON;


-- tables
-- Table: Connected_Words
-- Words that most often appear on the same page as the chosen word.

CREATE TABLE IF NOT EXISTS Connected_Words (
    Word_id integer NOT NULL,
    connection integer NOT NULL,
    count integer NOT NULL,
    CONSTRAINT Connected_Words_pk PRIMARY KEY (Word_id,connection),
    FOREIGN KEY (Word_id) REFERENCES Word (id),
    FOREIGN KEY (connection) REFERENCES Word (id)
);

-- Table: Page
CREATE TABLE IF NOT EXISTS Page (
    id integer NOT NULL  PRIMARY KEY,
    url varchar(2048) NOT NULL,
    shares integer,
    CONSTRAINT Page_ak_url UNIQUE (url)
);

-- Table: Page_Word
CREATE TABLE IF NOT EXISTS Page_Word (
    Word_id integer NOT NULL,
    Page_id integer NOT NULL,
    count integer NOT NULL,
    CONSTRAINT Page_Word_pk PRIMARY KEY (Word_id,Page_id),
    FOREIGN KEY (Word_id) REFERENCES Word (id),
    FOREIGN KEY (Page_id) REFERENCES Page (id)
);

-- Table: Word
CREATE TABLE IF NOT EXISTS Word (
    id integer NOT NULL  PRIMARY KEY,
    word varchar(100) NOT NULL,
    CONSTRAINT Words_ak_word UNIQUE (word)
);





-- End of file.

