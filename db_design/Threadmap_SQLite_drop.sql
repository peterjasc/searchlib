-- Created by Vertabelo (http://vertabelo.com)
-- Script type: drop
-- Scope: [tables, references, sequences, views, procedures]
-- Generated at Sun Oct 26 09:24:50 UTC 2014



-- tables
-- Table: Connected_Words
DROP TABLE IF EXISTS Connected_Words;
-- Table: Page
DROP TABLE IF EXISTS Page;
-- Table: Page_Word
DROP TABLE IF EXISTS Page_Word;
-- Table: Word
DROP TABLE IF EXISTS Word;



-- End of file.

