package com.threadmap.SearchLib;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.threadmap.SearchLib.database.DatabaseFactory;
import com.threadmap.SearchLib.database.IWriter;
import com.threadmap.SearchLib.wordcount.WordCount;

public class Crawler implements Runnable {
	
	private Thread t;
	private String threadName;

	private UrlValidator urlValidator;
	static LinkedList<String> domainUrls = new LinkedList<String>();
	LinkedList<String> toBeAddedUrls;
	LinkedList<String> toBeCrawledUrls;
	static HashSet<String> alreadyAddedUrls = new HashSet<String>();
	Integer urlShares;
	private List<WordCount> words;
	private static final int HOW_MANY_POPULAR_WORDS_TO_GET = 40;
	private static final int HOW_MANY_URLS_TO_CRAWL_BEFORE_SLEEP = 5;
	private static final int MILLISECONDS_TO_REST_BEFORE_CONT_CRAWL = 60000;
	private static final int LINKS_TO_CRAWL = 100;
	private static BitSet badUriChars = new BitSet(256);
	
	
	static {
	    badUriChars.set(0, 255, true);
	    badUriChars.andNot(org.apache.commons.httpclient.URI.unwise);
	    badUriChars.andNot(org.apache.commons.httpclient.URI.space);
	    badUriChars.andNot(org.apache.commons.httpclient.URI.control);
	    badUriChars.set('<', false);
	    badUriChars.set('>', false);
	    badUriChars.set('"', false);
	}
	
	public Crawler() {
		this.urlValidator = new UrlValidator();
		this.toBeAddedUrls = new LinkedList<String>();
		this.toBeCrawledUrls = new LinkedList<String>();
	}
	
	String extractLinksToCache(String crawledUrl) throws IOException {
	    
		String result = "";
		Document doc = Jsoup.connect(crawledUrl).get();
		Elements links = doc.select("a[href]");
		for (Element link : links) {
		String absoluteLinkInCrawledPage = link.absUrl("href");
			if (isValidLink(crawledUrl, absoluteLinkInCrawledPage)) {
				absoluteLinkInCrawledPage = cleanlLink(absoluteLinkInCrawledPage);
				toBeAddedUrls.add(absoluteLinkInCrawledPage);
			}
		}
		return result;
	}
	
	String cleanlLink(String url) throws IOException {
		String cleanedUrl;
		String domainName = getUrlDomainName(url);
		String path = getUrlPath(url);
		cleanedUrl = addHttp(domainName + path);
		return cleanedUrl;
	}
	
	static String addHttp(String url) {
		return "http://" + url;
	}
	
	// todo:should return cleaned link
	boolean isValidLink(String crawledUrl, String linkInUrlPage) throws IOException {
		
		String cleanedLink = null;
		try {
		cleanedLink = cleanlLink(linkInUrlPage);
		} catch (NullPointerException e) {
			e.printStackTrace();
			return false;
		}
		synchronized (alreadyAddedUrls) {
			if (alreadyAddedUrls.contains(linkInUrlPage)) {
				return false;
			} 
		}
		
		String domainName = getUrlDomainName(crawledUrl);
		if (linkInUrlPage.length() > 0 && 
				urlValidator.isValid(linkInUrlPage)){
			
			if (cleanedLink.contains(domainName)) {
				synchronized (alreadyAddedUrls) {
					alreadyAddedUrls.add(cleanedLink);
				}
			return true;
			} else {
				synchronized (domainUrls) {
//					System.out.println("DOMAINURLS:");
//					System.out.println(domainUrls);
					
					synchronized (alreadyAddedUrls) {
						if (!alreadyAddedUrls.contains(cleanedLink)) {
//							System.out.println("domainName " + addHttp(getUrlDomainName(cleanedLink)));
							synchronized (domainUrls) {
								domainUrls.add(addHttp(getUrlDomainName(cleanedLink)));
								return true;
							}
						}
					}
				}
			} 
			
		}
		return false;
	}
	
	static String getUrlDomainName(String url) {
	    URI uri = null;
		try {
			url = URIUtil.encode(url,badUriChars, "UTF-8");
			uri = new URI(url);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (URIException e) {
			e.printStackTrace();
		}
	    String domain = uri.getHost();
	    if (domain.startsWith("www.")) {
	    	domain = domain.substring(4);
	    }
	    return domain;
	}
	
	static String getUrlPath(String url) {
	    URI uri = null;
		try {
			url = URIUtil.encode(url,badUriChars, "UTF-8");
			uri = new URI(url);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (URIException e) {
			e.printStackTrace();
		}
	    String path = uri.getPath();
	    if(path.length() == 1) {
	    	path = "";
	    }
	    return path;
	}
	
	synchronized void addLinkFromCacheToDB(IWriter writer, String urlToBeAddedToDB) 
			throws IOException {
		/*System.out.println(urlToBeAddedToDB);*/
		if (writer.insertURLDataToDB(urlToBeAddedToDB,urlShares,words)) {
			toBeCrawledUrls.add(urlToBeAddedToDB);
		}
	}
	
	private String getPageData() throws IOException {
		
		String urlToBeAddedToDB = toBeAddedUrls.getFirst();
		toBeAddedUrls.removeFirst();
		IPageInfo pI = PageInfoFactory.create(urlToBeAddedToDB);
		urlShares = pI.getSocialInfo().getShareCount();
		List<WordCount> popularWords = pI.countWords(HOW_MANY_POPULAR_WORDS_TO_GET);
		words = changeCountToImportancePercentage(popularWords);
		return urlToBeAddedToDB;
	}
	
	private List<WordCount> changeCountToImportancePercentage(List<WordCount> words) {
		
		for (int i = 0; i < words.size(); i++) {
			if (i < 10) {
				words.get(i).setCount(100);
			} else if (i < 20) {
				words.get(i).setCount(90);
			} else if (i < 30) {
				words.get(i).setCount(80);
			} else if (i <= 40) {
				words.get(i).setCount(70);
			}
		}
		return words;
	}

	private synchronized void addToBeAddedLinksToDB(int toBeAddedUrlsSize)
			throws IOException, InterruptedException {
		String urlToBeAddedToDB;
		for (int i = 0; i < toBeAddedUrlsSize; i++) {
			if (i != 0 && i % HOW_MANY_URLS_TO_CRAWL_BEFORE_SLEEP == 0) {
				System.out.println("SOME WOLOLO MAGIC, NUMBER:" + i);
  				Thread.sleep(MILLISECONDS_TO_REST_BEFORE_CONT_CRAWL);
  				Thread.yield();
  			}
			urlToBeAddedToDB = getPageData();
			if (words.size() != 0 && urlShares != 0) {
				try (IWriter writer = DatabaseFactory.createWriter()) {
					addLinkFromCacheToDB(writer, urlToBeAddedToDB);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public synchronized void saveDomainsToFile(String fileName) {
		
		BufferedWriter br = null;
		try {
			br = new BufferedWriter(new FileWriter(fileName));
			Iterator<String> domainUrlsIterator = domainUrls.iterator();
	        while (domainUrlsIterator.hasNext()) {
	        	br.write(domainUrlsIterator.next());
	        }
	        
	    } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Crawler(String threadName) throws IOException, InterruptedException {
		
		this();
		this.threadName = threadName;
		
	}
	
	public void run() {
		
		 System.out.println("Running " +  threadName );
		 String url = "http://www.reddit.com";
	      try {
	    	boolean wait = false;
	    	url = cleanlLink(url);
			
	    	while (true) {
	    		synchronized (alreadyAddedUrls) {
			  		synchronized (domainUrls) {
			  			if (domainUrls.isEmpty() && 
			  					!alreadyAddedUrls.contains(url)) {
			  				alreadyAddedUrls.add(url);
			  				break;
			  			} else if (!domainUrls.isEmpty()) {
			  				url = domainUrls.removeLast();
			  				break;
			  			}
			  		}
			  		if(alreadyAddedUrls.contains(url)) {
		  		    	wait = true;
	  		    	}
	    		}
	    		if (wait) {
	    			wait = false;
	    			Thread.sleep(60000);	
	    		}
	    	}
	  		int howManyLinks = LINKS_TO_CRAWL;
	  		while (howManyLinks > 0) {
	  			extractLinksToCache(url);
	  			int toBeAddedUrlsSize = toBeAddedUrls.size();
	  			addToBeAddedLinksToDB(toBeAddedUrlsSize);
	  			
	  			if (!toBeCrawledUrls.isEmpty()){
	  			url = toBeCrawledUrls.removeFirst();
	  			} else {
	  				break;
	  			}
	  			howManyLinks--;
	  		}
	     } catch (InterruptedException e) {
	         System.out.println("Thread " +  threadName + " interrupted.");
	     } catch (IOException e) {
			e.printStackTrace();
		}
	     System.out.println("Thread " +  threadName + " exiting. The url was: " + url);
	}
	
	public void start () {
	      if (t == null)
	      {
	         t = new Thread (this, threadName);
	         t.start();
	      }
	   }
	
}

