package com.threadmap.SearchLib;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class CreateCrawlerThreads {
	
	public static void main(String args[]) {
		
		try {
			for (int i = 0; i < 600; i++) {
				new Crawler("Thread " + i).start();
			}
		} catch (InterruptedException e) {
			 System.out.println("Crawler interrupted.");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 

	}
	   
   
	
}