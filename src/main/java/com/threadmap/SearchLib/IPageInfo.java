/**
 * 
 */
package com.threadmap.SearchLib;

import java.io.IOException;
import java.util.List;

import com.threadmap.SearchLib.wordcount.WordCount;

/**
 * @author karmo
 */
public interface IPageInfo {
	
	/**
	 * Counts the number of occurrences of each word and
	 * returns given number of most popular ones.
	 * @param resultSize maximum result size.
	 * @return sorted list (most popular first)
	 * @throws IOException 
	 */
	List<WordCount> countWords(int resultSize) throws IOException;
	SocialInfo getSocialInfo() throws IOException;
	
}
