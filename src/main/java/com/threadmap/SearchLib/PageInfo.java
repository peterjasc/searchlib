/**
 * 
 */
package com.threadmap.SearchLib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.threadmap.SearchLib.apirequest.IAPIRequest;
import com.threadmap.SearchLib.apirequest.IAPIRequestResultObject;
import com.threadmap.SearchLib.wordcount.HtmlFilter;
import com.threadmap.SearchLib.wordcount.WordCount;
import com.threadmap.SearchLib.wordcount.WordCounter;
import com.threadmap.SearchLib.wordcount.WordFilter;


/**
 * @author karmo
 */
public class PageInfo implements IPageInfo {
	
	private String url;
	private WordCounter wordCounter = new WordCounter();
	private WordFilter wordFilter = new WordFilter("Conjunctions.txt");
	private IAPIRequest fbAPIReq;
	
	public PageInfo(String url, IAPIRequest fbAPIReq) {
		this.url = url;
		this.fbAPIReq = fbAPIReq;
	}
	
	public List<WordCount> countWords(int resultSize) throws IOException {
		String content = HtmlFilter.html2text(url);
		if (content == "") {
			return new ArrayList<WordCount>();
		}
		List<WordCount> list = wordFilter.filter(wordCounter.findPopular(content));
		if (list.size() > resultSize)
			return new ArrayList<WordCount>(list.subList(0, resultSize));
		else
			return list;
	}
	
	public SocialInfo getSocialInfo () throws IOException {
		IAPIRequestResultObject resultObject = fbAPIReq.getResultObject(this.url);
		return new SocialInfo(resultObject.getShares());
	}
}