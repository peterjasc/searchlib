/**
 * 
 */
package com.threadmap.SearchLib;

import com.threadmap.SearchLib.apirequest.APIRequest;

/**
 * @author karmo
 * Class to create new IPageInfo objects.
 */
public class PageInfoFactory {

	/**
	 * @param url Address of the web page. Should start with "http://".
	 */
	public static IPageInfo create(String url) {
		APIRequest apiRequest = new APIRequest("https://graph.facebook.com/"
				+ "?access_token=336507393190722"
				+ "|1753ff7b90396d31c83f5b103821b061"
				+ "&fields=share{share_count}&id=");
		return new PageInfo(url, apiRequest);
	}
	
}
