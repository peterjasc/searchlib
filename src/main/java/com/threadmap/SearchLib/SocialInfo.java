/**
 * 
 */
package com.threadmap.SearchLib;

/**
 * @author peter
 */
public class SocialInfo {
	
	private int shareCount;
	
	public SocialInfo (Integer shareCount) {
		this.shareCount = shareCount;
	}
	
	public Integer getShareCount() {
		return shareCount;
	}

}
