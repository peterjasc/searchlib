package com.threadmap.SearchLib.apirequest;

import java.util.Map;
import com.google.gson.Gson;

/** Custom Exception to return a JSON encoded error 
 * message when dealing with the Facebook API.
 * @author Shane Chism (schism at acm dot org) 
 */
public class APIException extends Exception {

	private String errMessage;
	private static final long serialVersionUID = 5560904358324172135L;
	public APIException( String response ){
		
		Gson gson = new Gson();
		try {
			/* An example error string: 
			 {"error":{"message":"Unknown path components:
			\/1106460174","type":"OAuthException"}}
		    "error" is the equivalent of a Java Map Object */
			FBError requestError = gson.fromJson( response, FBError.class );
			
			// Assign the appropriate map values to our exception class' variables
			errMessage = requestError.error.get("message");
			
		} catch( Exception e ) {
			errMessage = "An unkown error has occured."
					+ " Unable to process response from server: \n" + response;
		}
	}
	
	public void show(){
		System.out.println( this.errMessage );
	}
	
	/** Container for the JSON parse of a server returned error. 
	 * JSON elements in the array are parsed to this.
	 * @author Shane Chism (schism at acm dot org) 
	 */
	private class FBError {
		public Map<String,String> error;
	}
	
}