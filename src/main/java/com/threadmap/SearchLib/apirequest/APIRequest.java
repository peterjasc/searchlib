package com.threadmap.SearchLib.apirequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.google.gson.Gson;

/** The FB API Request.
 */
public class APIRequest implements IAPIRequest {
	
	private String fbGraphApiUrl;
	
	public APIRequest(String fbGraphApiUrl) {
		this.fbGraphApiUrl = fbGraphApiUrl;
	}
	
	/** Get shares for an URL.
	 * @param URLID - the URL for which we want to get the shares for
	 * @throws IOException 
	 * @throws Exception 
	 */
	public APIRequestResultObject getResultObject( String URLID ) throws IOException {
		
		String json = "";
		APIRequestResultObject requestResult = null;
		
		try {
			URLConnection fbAPIRequest = new URL(fbGraphApiUrl 
					+ URLID).openConnection();
			HttpURLConnection fbAPIRequestHttpURL = ((HttpURLConnection)fbAPIRequest);
			
			if(fbAPIRequestHttpURL.getResponseCode() != 200){
				json = throwAPIException(json, fbAPIRequestHttpURL);
			}
			
			String input;
			BufferedReader reader = readRequestWithReader(fbAPIRequest);
			while( (input = reader.readLine()) != null) {
				json += input;
			}
			reader.close();
			requestResult = returnRequestResultObjectWithGson(json);
			
		} catch(APIException e) {
			e.show();
		}	
		
		return requestResult;	
	}

	private BufferedReader readRequestWithReader(URLConnection fbAPIRequest)
			throws IOException {
		BufferedReader reader = new BufferedReader
				(new InputStreamReader(fbAPIRequest.getInputStream()));
		return reader;
	}

	private APIRequestResultObject returnRequestResultObjectWithGson(String json) {
		APIRequestResultObject requestResult;
		Gson gson = new Gson();
		requestResult =
				gson.fromJson(json, APIRequestResultObject.class);
		return requestResult;
	}

	private String throwAPIException(String json,
			HttpURLConnection fbAPIRequestHttpURL) throws IOException, APIException {
		BufferedReader reader = new BufferedReader( 
				new InputStreamReader(fbAPIRequestHttpURL.getErrorStream()));
		String input;
		while( (input = reader.readLine()) != null) {
			json += input;
		}
		reader.close();
		throw new APIException(json);
	}
	
	
}
	