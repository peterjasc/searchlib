package com.threadmap.SearchLib.apirequest;

import java.util.Map;

/** The JSON result for the API request.
 * Example: { "share": {"share_count": 3640}, "id": "http://www.postimees.ee"}
 */
public class APIRequestResultObject implements IAPIRequestResultObject {
	
	private Map<String,String> share;
	Integer shareCount = null;
	
	public Integer getShares() { 
		
		try {
		//	System.out.print(share);
		if (share != null) {
			shareCount = Integer.valueOf(share.get("share_count"));
		} else {
			shareCount = 0;
		}
		
		} catch (NumberFormatException numberEx) {
			System.out.println(numberEx + " -- "
					+ " a problem with Facebook API Protocol");
		}
		return shareCount;
		}
}