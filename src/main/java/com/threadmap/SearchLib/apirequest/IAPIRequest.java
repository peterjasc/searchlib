package com.threadmap.SearchLib.apirequest;

import java.io.IOException;

public interface IAPIRequest {
	
public IAPIRequestResultObject getResultObject( String URLID ) throws IOException;
	
	
}
