/**
 * 
 */
package com.threadmap.SearchLib.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.threadmap.SearchLib.wordcount.WordCount;

/**
 * @author karmo
 */
public class DBWriter implements IWriter {

	private Database db;
	private PreparedStatement insertToConnectedWords, updateConnectedWords,
	checkUrlId, selectUrlId, selectWordId, insertToPageTable, insertIntoPageWordTable,
	insertWordIntoDB;
	
	String url;
	int shareCount;
	List<WordCount> words;
	List<Integer> wordIDs;
	
	public DBWriter(Database database) throws SQLException {
		db = database;
		insertToConnectedWords = prepareInsertToConnectedWords();
		updateConnectedWords = prepareUpdateConnectedWords();
		checkUrlId = prepareCheckUrlId();
		selectUrlId = prepareSelectUrlId();
		insertToPageTable = prepareInsertToPageTable();
		selectWordId = prepareSelectWordId();
		insertIntoPageWordTable = prepareInsertIntoPageWordTable();
		insertWordIntoDB = prepareInsertWordIntoDB();
	}
	
	@Override
	public void close() throws Exception {
		insertToConnectedWords.close();
		updateConnectedWords.close();
		checkUrlId.close();
		selectUrlId.close();
		selectWordId.close();
		insertToPageTable.close();
		insertIntoPageWordTable.close();
		insertWordIntoDB.close();
		db.closeConnection();
	}

	
	public boolean insertURLDataToDB(String url, int shareCount, List<WordCount> words) {
		this.url = url;
		this.shareCount = shareCount;
		this.words = words;
		try {
			return addLinkIfNotExistsInDB();
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return false;
	}
	
	void connectWordsWithUrlInDB(int pageID) throws SQLException {
		for (int i = 0; i < wordIDs.size(); i++) {
			Integer wordID = wordIDs.get(i);
			Integer count = words.get(i).getCount();
			insertIntoPageWordTable(wordID, pageID, count);
		}
	}
	
	/**
	 * Inserts a list of words to DB.
	 * @param list
	 * @return List of word IDs in DB.
	 * @throws SQLException
	 */
	public List<Integer> insertWords() throws SQLException {
		List<Integer> results = new ArrayList<>();
		for (WordCount wordCount : words)
			results.add(insertWordGetID(wordCount.getWord()));
		return results;
	}

	int insertWordGetID(String word) throws SQLException {
		if (!db.WordExistsInDB(word))
			return insertWordIntoDB(word);
		else
			return readWordID(word);
	}

	public void connectWords() 
			throws SQLException {
		int[][] counts = calculateConnectionCounts();
		insertWordConnections(counts);
	}

	private int[][] calculateConnectionCounts() {
		int[][] counts = new int[wordIDs.size()][];
		for (int i = 0; i < wordIDs.size(); i++)
			counts[i] = new int[wordIDs.size()];
		for (int i = 0; i < wordIDs.size(); i++) {
			for (int j = 0; j < wordIDs.size(); j++) {
				counts[i][j] = words.get(i).getCount() * words.get(j).getCount();
			}
		}
		return counts;
	}
	
	private PreparedStatement prepareSelectUrlId() throws SQLException {
		return db.prepareStatement("SELECT id FROM Page WHERE url = ? limit 1;");
	}
	
	private PreparedStatement prepareInsertIntoPageWordTable() throws SQLException {
		return db.prepareStatement("INSERT INTO Page_Word (Word_id,Page_id,count) " +
				"VALUES (?, ?, ?);");
	}
	
	private PreparedStatement prepareInsertWordIntoDB() throws SQLException {
		return db.prepareStatement("INSERT INTO Word (word) " +
				"VALUES (?);");
	}
	
	private PreparedStatement prepareCheckUrlId() throws SQLException {
		return db.prepareStatement("SELECT 1 FROM Page WHERE url = ? limit 1;");
	}
	
	private PreparedStatement prepareSelectWordId() throws SQLException {
		return db.prepareStatement("SELECT id FROM Word WHERE Word = ? limit 1;");
	}
	
	private PreparedStatement prepareInsertToPageTable() throws SQLException {
		return db.prepareStatement("INSERT INTO Page (url, shares )"
				+ "VALUES (?, ?);");
	}
	
	private PreparedStatement prepareInsertToConnectedWords()
			throws SQLException {
		return db.prepareStatement("INSERT INTO Connected_Words "
				+ "(Word_id, connection, count) VALUES (?, ?, ?);");
	}
	
	private PreparedStatement prepareUpdateConnectedWords()
			throws SQLException {
		return db.prepareStatement("UPDATE Connected_Words SET count = count + ?"
				+ "WHERE Word_id = ? AND connection = ?;");
	}
	
	private void insertWordConnections(int[][] counts)
			throws SQLException {
		for (int i = 0; i < wordIDs.size(); i++) {
			for (int j = 0; j < wordIDs.size(); j++) {
				if (i != j) {
					int affectedRows = increaseCountIfRowExists(counts, i, j);
					if (affectedRows == 0)
						insertRowToConnectedWords(counts, i, j);
				}
			}
		}
	}

	private void insertRowToConnectedWords(int[][] counts, int i, int j) 
			throws SQLException {
		int index = 1;
		insertToConnectedWords.setInt(index++, wordIDs.get(i));
		insertToConnectedWords.setInt(index++, wordIDs.get(j));
		insertToConnectedWords.setInt(index++, counts[i][j]);
		insertToConnectedWords.executeUpdate();
	}

	/**
	 * Updates existing row in Connected_Words.
	 * @return Number of affected rows.
	 * @throws SQLException
	 */
	private int increaseCountIfRowExists(int[][] counts, int i, int j) 
			throws SQLException {
		int index = 1;
		updateConnectedWords.setInt(index++, counts[i][j]);
		updateConnectedWords.setInt(index++, wordIDs.get(i));
		updateConnectedWords.setInt(index++, wordIDs.get(j));
		return updateConnectedWords.executeUpdate();
	}

	boolean addLinkIfNotExistsInDB() throws SQLException {
		int index = 1;
		checkUrlId.setString(index++, url);
		try (ResultSet checkLink = checkUrlId.executeQuery()) {
			if (!checkLink.next()) {
				int urlId = insertToPageTable();
				this.wordIDs = insertWords();
				connectWordsWithUrlInDB(urlId);
				connectWords();
				db.commit();
				return true;
			}
		}
		return false;
	}
	
	Integer insertToPageTable()
			throws SQLException {
		int index = 1;
		insertToPageTable.setString(index++, url);
		insertToPageTable.setInt(index++, shareCount);
		insertToPageTable.executeUpdate();
		selectUrlId.setString(1, url);
		try (ResultSet urlId = selectUrlId.executeQuery()) {
			return urlId.getInt("id");
		}
	}
	
	int readWordID(String word) throws SQLException {
		selectWordId.setString(1, word);
		try (ResultSet selectWordID = selectWordId.executeQuery()) {
			return selectWordID.getInt("id");
		}
	}
	
	void insertIntoPageWordTable(int word_id, int page_id, int count)
			throws SQLException {
		int index = 1;
		insertIntoPageWordTable.setInt(index++, word_id);
		insertIntoPageWordTable.setInt(index++, page_id);
		insertIntoPageWordTable.setInt(index++, count);
		insertIntoPageWordTable.executeUpdate();
	}
	
	Integer insertWordIntoDB(String word) throws SQLException {
		insertWordIntoDB.setString(1, word);
		insertWordIntoDB.executeUpdate();
		return readWordID(word);
	}
}
