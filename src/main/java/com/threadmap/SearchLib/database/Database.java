package com.threadmap.SearchLib.database;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {

	private Statement stmt = null;
	Connection c = null;
	private PreparedStatement WordExistsInDB;
	private String statementWordExistsInDB;
	
	private static final String DROP_STATEMENTS = "db_design/Threadmap_SQLite_drop.sql";
	private static final String CREATE_STATEMENTS = "db_design/Threadmap_SQLite_create.sql";

	public Database(String dbName, boolean reCreateTables) throws Exception {
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
		stmt = c.createStatement();
		
		if (reCreateTables) 
			runStatementsFile(dbName, DROP_STATEMENTS);
		runStatementsFile(dbName, CREATE_STATEMENTS);
		
		c.setAutoCommit(false);
		statementWordExistsInDB = "SELECT 1 FROM Word "
				+ "WHERE word = ? limit 1;";
		WordExistsInDB = prepareStatement(statementWordExistsInDB);
		System.out.println("Opened database successfully");
	}
	
	public void closeConnection() {
		try {
			commit();
			stmt.close();
			c.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}
	
	public void commit() throws SQLException {
		c.commit();
	}
	
	public Statement getStatement() {
		return stmt;
	}
	
	public PreparedStatement prepareStatement(String statement) throws SQLException {
		return c.prepareStatement(statement);
	}

	public void runStatementsFile(String dbName, String statementFile) throws IOException, SQLException {
		String statements = fileToString(statementFile);
		for (String s : statements.split(";"))
			stmt.executeUpdate(s);
	}

	private String fileToString(String dbName) throws IOException {
		String result = "", line; 
		try (BufferedReader bf = new BufferedReader(new FileReader(dbName))) {
			while ((line = bf.readLine()) != null) {
				if (!line.startsWith("--"))
					result += line + '\n';
			}
		}
		return result;
	}
	
	boolean WordExistsInDB(String word) throws SQLException {
		WordExistsInDB.setString(1, word);
		try (ResultSet wordExists = WordExistsInDB.executeQuery()) {
			return wordExists.next();
		}
	}

}
