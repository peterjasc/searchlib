package com.threadmap.SearchLib.database;


public class DatabaseFactory {

	public static ISearchResults createSearcher(int resultSize) throws Exception {
		Database db = new Database("threadmap.db", false);
		return new SearchResults(db, resultSize);
	}
	
	public static IWriter createWriter() throws Exception {
		Database db = new Database("threadmap.db", false);
		return new DBWriter(db);
	}
}
