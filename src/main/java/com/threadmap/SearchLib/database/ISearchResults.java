package com.threadmap.SearchLib.database;

import java.util.List;

import com.threadmap.SearchLib.wordcount.WordCount;


public interface ISearchResults extends AutoCloseable {
	Results search(String word);
	List<WordCount> findConnected(String word);
	List<WordCount> findMostPopular();
}
