/**
 * 
 */
package com.threadmap.SearchLib.database;

import java.util.List;

import com.threadmap.SearchLib.wordcount.WordCount;

/**
 * @author karmo
 */
public interface IWriter extends AutoCloseable {
	boolean insertURLDataToDB(String url, int shareCount, List<WordCount> words);
}
