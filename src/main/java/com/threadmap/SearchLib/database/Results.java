/**
 * 
 */
package com.threadmap.SearchLib.database;

import java.util.ArrayList;
import java.util.List;

/**
 * @author karmo
 */
public class Results {
	public List<Integer> counts = new ArrayList<Integer>();
	public List<Integer> shares = new ArrayList<Integer>();
	public List<String> urls = new ArrayList<String>();
}
