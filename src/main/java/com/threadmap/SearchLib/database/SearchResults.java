package com.threadmap.SearchLib.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.threadmap.SearchLib.wordcount.WordCleaner;
import com.threadmap.SearchLib.wordcount.WordCount;


public class SearchResults implements ISearchResults {
	
	private Database db;
	private Statement stmt;
	private int resultSize;
	private int dimension;
	
	public SearchResults(Database database, int resultSize) throws SQLException {
		db = database;
		stmt = db.getStatement();
		this.resultSize = resultSize;
	}
	
	@Override
	public void close() throws Exception {
		db.closeConnection();
	}

	@Override
	public Results search(String line) {
		String[] words = splitByWhitespace(line);
		WordCleaner wordCleaner = new WordCleaner();
		words = cleanArrayWords(words, wordCleaner);	
		words = removeUnwantedWords(words, wordCleaner);
		
		try {
			return runSearchQuery(words);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String[] cleanArrayWords(String[] array, WordCleaner wordCleaner) {
		dimension = 0;
		
		for(int i = 0; i < array.length; i++) {
			wordCleaner.processWord(array[i]);		
			if(!wordCleaner.isTooShort(array[i])) {
				dimension++;
			}
		}
		
		return array;
	}
	
	private String[] removeUnwantedWords(String[] array, WordCleaner wordCleaner) {
		String[] temp = new String[dimension];
		int index = 0;
		
		for(int i = 0; i < array.length; i++) {
			if(!wordCleaner.isTooShort(array[i])) {
				temp[index] = array[i];
				index++;
			}
		}
		
		return temp;
	}

	private String[] splitByWhitespace(String line) {
		return line.split("\\s+");
	}
	
	private Results runSearchQuery(String[] words) throws SQLException {
		PreparedStatement searchQuery = prepareSearchStmt(words.length);
		for (int i = 0; i < words.length; i++) 
			searchQuery.setString(i + 1, words[i]);
		try (ResultSet wordPage = searchQuery.executeQuery()) {
			return loopThroughResults(wordPage);
		}
	}
	
	private PreparedStatement prepareSearchStmt(int noOfArgs) throws SQLException {
		String args = makeArgs(noOfArgs);
		String query = String.format(
				"SELECT sum(PW.count) AS count_sum, P.shares, P.url"
				+ " FROM Page_Word PW"
				+ " INNER JOIN Word W ON W.id = Word_id"
				+ " INNER JOIN Page P ON P.id = Page_id"
				+ " WHERE W.word IN (%s)"
				+ " GROUP BY (P.id)"
				+ " ORDER BY count(*) DESC, sum(PW.count * P.shares) DESC"
				+ " LIMIT %d;",
				args, resultSize);
		return db.prepareStatement(query);
	}

	private Results loopThroughResults(ResultSet wordPage) throws SQLException {
		Results results = new Results();
		while (wordPage.next()) {
			results.counts.add(wordPage.getInt("count_sum"));
			results.shares.add(wordPage.getInt("shares"));
			results.urls.add(wordPage.getString("url"));
		}
		return results;
	}
	
	@Override
	public List<WordCount> findConnected(String line) {
		String[] words = splitByWhitespace(line);
		try {
			return runFindConnectedQuery(words);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<WordCount> runFindConnectedQuery(String[] words)
			throws SQLException {
		PreparedStatement findConnectedQuery = prepareFindConnectedStmt(words.length);
		bindValues(findConnectedQuery, words);
		try (ResultSet wordPage = findConnectedQuery.executeQuery()) {
			return makeConnectedList(wordPage);
		}
	}

	private void bindValues(PreparedStatement findConnectedQuery, String[] words)
			throws SQLException {
		int index = 1;
		for (int j = 0; j < 2; j++)
			for (int i = 0; i < words.length; i++) 
				findConnectedQuery.setString(index++, words[i]);
	}

	private List<WordCount> makeConnectedList(ResultSet wordPage)
			throws SQLException {
		List<WordCount> list = new ArrayList<>();
		while (wordPage.next()) {
			String w = wordPage.getString(1);
			int count = wordPage.getInt(2);
			list.add(new WordCount(w, count));
		}
		return list;
	}

	private PreparedStatement prepareFindConnectedStmt(int noOfArgs) throws SQLException {
		String args = makeArgs(noOfArgs);
		String query = String.format(
				"SELECT word, sum(count)"
				+ " FROM Word INNER JOIN ("
				+ " SELECT connection, count"
				+ " FROM Connected_Words"
				+ " INNER JOIN Word W ON W.id = Word_id"
				+ " WHERE W.word IN (%s)"
				+ ") I ON I.connection = id"
				+ " WHERE word NOT IN (%s)"
				+ " GROUP BY connection"
				+ " ORDER BY sum(count) DESC LIMIT %d;",
				args, args, resultSize);
		return db.prepareStatement(query);
	}

	private String makeArgs(int noOfArgs) {
		String args = "?";
		for (int i = 1; i < noOfArgs; i++)
			args += ", ?";
		return args;
	}

	@Override
	public List<WordCount> findMostPopular() {
		String query = getFindPopularQuery();
		try (ResultSet wordPage = stmt.executeQuery(query)) {
			return makeConnectedList(wordPage);
		} catch (SQLException e1) {
			System.err.println(e1.getClass().getName() + ": " + e1.getMessage());
		}
		return null;
	}
	
	private String getFindPopularQuery() {
		return "SELECT word, sum FROM Word INNER JOIN ("
				+ "SELECT Word_id, Sum(count) AS sum FROM Page_Word "
				+ "GROUP BY Word_id ORDER BY sum DESC "
				+ "LIMIT " + resultSize
				+ ") ON id = Word_id;";
	}
	
}
