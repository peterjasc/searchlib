/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;

/**
 * @author karmo
 *
 */
public class HtmlFilter {
	
	/**
	 * Removes html tags from web page source.
	 * @param address Address of the web page
	 * @throws IOException
	 */
	public static String html2text(String address) throws IOException {
		URL url = new URL(address);
		try {
	    return Jsoup.parse(url, 9000).text();
		} catch (Exception e) {
			System.out.println("Jsoup parse error, url:" + url + ".");
			e.printStackTrace();
		}
		return "";
	}
	
}
