package com.threadmap.SearchLib.wordcount;

public class WordCleaner {
	
	public String processWord(String word) {
		return word.replaceAll("[^\\p{L}]+", "").toLowerCase();
	}

	public boolean isTooShort(String word) {
		return word.equals("") || word.length() < 3;
	}
}
