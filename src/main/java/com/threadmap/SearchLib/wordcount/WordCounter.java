/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author karmo
 *
 */
public class WordCounter {
	
	HashMap<String, Counter> map = new HashMap<String, Counter>();
	
	/**
	 * Finds words with most occurrences in input.
	 * @param input Text to parse.
	 * @return Sorted list with each word and its count (most popular first).
	 */
	public List<WordCount> findPopular(String input) {
		Map<String, Counter> counts = count(input);
		List<WordCount> list = mapToList(counts);
		sortByCount(list);
		return list;
	}
	
	/**
	 * Counts the number of occurrences of each word.
	 * @param input String to process.
	 * @return Map where key is a word and value the number of occurrences.
	 */
	public Map<String, Counter> count(String input) {
		map.clear();
		if (input == null) return map;
		WordCleaner wordCleaner = new WordCleaner();
		
		String[] ar = input.split("\\s");
		for (int i = 0; i < ar.length; i++) {
			ar[i] = wordCleaner.processWord(ar[i]);
			if (wordCleaner.isTooShort(ar[i])) continue;
			countTheWord(ar, i);
		}
		return map;
	}

	private Counter countTheWord(String[] ar, int i) {
		Counter prev = map.get(ar[i]);
		if (prev == null)
			map.put(ar[i], new Counter());
		else
			prev.increment();
		return prev;
	}

	List<WordCount> mapToList(Map<String, Counter> counts) {
		List<WordCount> list = new ArrayList<>();
		for (Map.Entry<String, Counter> entry : counts.entrySet()) {
			WordCount wc = new WordCount();
		    wc.word = entry.getKey();
		    wc.count = entry.getValue().intValue();
		    list.add(wc);
		}
		return list;
	}

	private void sortByCount(List<WordCount> list) {
		Collections.sort(list, new Comparator<WordCount>() {
			@Override
			public int compare(WordCount o1, WordCount o2) {
				return o2.count - o1.count;
			}
		});
	}

	/**
	 * Class for incrementing an int after it has been put to a Map.
	 * @author karmo
	 */
	class Counter {
		private int value = 1;
		void increment() { value++; }
		int intValue() { return value; }
	}
	
}
