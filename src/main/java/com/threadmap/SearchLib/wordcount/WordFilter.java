/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author karmo
 */
public class WordFilter {
	
	Set<String> excluded;
	
	/**
	 * @param exclusionFile File containing a list of words or phrases 
	 * to filter out (each on its own line)
	 */
	public WordFilter(String exclusionFile) {
		readExcluded(exclusionFile);
	}

	private void readExcluded(String fileName) {
		excluded = new HashSet<>();
		WordCleaner wordCleaner = new WordCleaner();
		try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
	        String line;
	        while ((line = br.readLine()) != null) {
	        	line = wordCleaner.processWord(line.toLowerCase().trim());
	        	
	        	if(!wordCleaner.isTooShort(line))
	            excluded.add(line);
	        }
	    } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes excluded words from input list.
	 * @param input List to filter.
	 * @return A copy without excluded words.
	 */
	public List<WordCount> filter(List<WordCount> input) {
		List<WordCount> result = new ArrayList<>();
		for (Iterator<WordCount> it = input.iterator(); it.hasNext();) {
			WordCount wc = it.next();
			if (!excluded.contains(wc.word.toLowerCase()))
				result.add(wc);
		}
		return result;
	}
	
}
