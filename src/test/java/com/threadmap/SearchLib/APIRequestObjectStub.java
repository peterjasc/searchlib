package com.threadmap.SearchLib;

import com.threadmap.SearchLib.apirequest.IAPIRequestResultObject;

public class APIRequestObjectStub implements IAPIRequestResultObject {
	
	int number;
	
	@Override
	public Integer getShares() {
		return number;
	}
	
	public APIRequestObjectStub(int number) {
		this.number = number;
	}

}
