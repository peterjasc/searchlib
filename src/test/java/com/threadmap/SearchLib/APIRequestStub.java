package com.threadmap.SearchLib;

import java.io.IOException;

import com.threadmap.SearchLib.apirequest.IAPIRequest;
import com.threadmap.SearchLib.apirequest.IAPIRequestResultObject;

public class APIRequestStub implements IAPIRequest {
	
	int number;
	
	@Override
	public IAPIRequestResultObject getResultObject(String URLID)
			throws IOException {
		return new APIRequestObjectStub(number);
	}
	
	public APIRequestStub(int number) {
		this.number = number;
	}
		
}
