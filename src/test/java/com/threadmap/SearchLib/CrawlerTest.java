package com.threadmap.SearchLib;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.bigtesting.fixd.Method;
import org.bigtesting.fixd.ServerFixture;
import org.junit.Test;

import com.threadmap.SearchLib.database.IWriter;
import com.threadmap.SearchLib.wordcount.WordCount;

public class CrawlerTest {
	
	public static int PORT = 8097;
	ServerFixture server;

	@Test
	public void testExtractLinksToCache() throws IOException {
		
		server = new ServerFixture(PORT);
		server.start();
		  
		String page = "/testingELTC";
		String response = "<a href=\"http://127.0.0.1/\" "
				+ "title=\"Localhost\"><strong>Localhost</strong>";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		Crawler c = new Crawler();
		c.extractLinksToCache("http://127.0.0.1:" + PORT + page);
		LinkedList<String> toBeAddedLinks = c.toBeAddedUrls;
		assertEquals(toBeAddedLinks.getFirst(), "http://127.0.0.1");
		
		server.stop();
	}
	
	@Test
	public void testExtractLinksToCacheWithoutAbsolute() throws IOException {
		
		server = new ServerFixture(PORT);
		server.start();
		  
		String page = "/testingELTC";
		String response = "<a href=\""+page+"\" "
				+ "title=\"Localhost\"><strong>Localhost</strong>";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		Crawler c = new Crawler();
		c.extractLinksToCache("http://127.0.0.1:" + PORT + page);
		LinkedList<String> toBeAddedLinks = c.toBeAddedUrls;
		assertEquals(toBeAddedLinks.getFirst(), "http://127.0.0.1/testingELTC");
		
		server.stop();
	}
	
	@Test
	public void testExtractLinksIsntValidURL() throws IOException {
		
		server = new ServerFixture(PORT);
		server.start();
		  
		String page = "/testingELTC";
		String response = "<a href=\""+page+"\" "
				+ "title=\"Localhost\"><strong>Localhost</strong>";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		Crawler c = new Crawler();
		c.extractLinksToCache("http://127.0.0.1:" + PORT + page);
		LinkedList<String> toBeAddedLinks = c.toBeAddedUrls;
		assertEquals(toBeAddedLinks.getFirst(), "http://127.0.0.1/testingELTC");
		
		server.stop();
	}
	
	@Test
	public void testExtractLinksToCacheFileLocation() throws IOException {
		
		server = new ServerFixture(PORT);
		server.start();
		  
		String page = "/testingELTC";
		String response = "<a href=\"file.jpg\" "
				+ "title=\"Localhost\"><strong>Localhost</strong>";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		Crawler c = new Crawler();
		c.extractLinksToCache("http://127.0.0.1:" + PORT + page);
		LinkedList<String> toBeAddedLinks = c.toBeAddedUrls;
		assertNotEquals(toBeAddedLinks.getFirst(), "http://127.0.0.1:8097/file.jpg");
		
		server.stop();
	}

	@Test
	public void testCleanlLink() throws IOException {
		String url = "http://127.0.0.1";
		Crawler c = new Crawler();
		assertEquals(c.cleanlLink(url), "http://127.0.0.1");
	}
	
	@Test
	public void testCleanlLink2() throws IOException {
		String url = "ftp://127.0.0.1";
		Crawler c = new Crawler();
		assertEquals(c.cleanlLink(url), "http://127.0.0.1");
	}

	@Test
	public void testIsValidLink() {
		String url = "http://127.0.0.1/more/andmore.jsp";
		Crawler c = new Crawler();
		assertEquals(c.isValidLink(url, url), true);
	}
	@Test
	public void testIsValidLinkEmptyString() {
		String url = "";
		String url2 = "http://127.0.0.1/more/andmore.jsp";
		Crawler c = new Crawler();
		assertEquals(c.isValidLink(url2, url), false);
	}
	
	@Test
	public void testIsValidLinkURLValidatorFail() {
		String url = "htre://127.4.0.0.1/more/andmore.jsp";
		String url2 = "http://127.0.0.1/more/andmore.jsp";
		Crawler c = new Crawler();
		assertEquals(c.isValidLink(url2, url), false);
	}
	
	@Test
	public void testIsValidLinkDoesntContainDomain() {
		String url = "http://128.0.0.1/more/andmore.jsp";
		String url2 = "http://127.0.0.1/more/andmore.jsp";
		Crawler c = new Crawler();
		assertEquals(c.isValidLink(url2, url), false);
	}
	
	
	@Test
	public void testIsValidLinkAlreadyAdded() {
		String url = "http://127.0.0.1/more/andmore.jsp";
		Crawler c = new Crawler();
		c.alreadyAddedUrls = new HashSet<String>();
		c.alreadyAddedUrls.add(url);
		assertEquals(c.isValidLink(url, url), false);
	}

	@Test
	public void testGetUrlDomainName() {
		String url = "http://127.0.0.1/more/andmore.jsp";
		assertEquals(Crawler.getUrlDomainName(url), "127.0.0.1");
	}
	
	@Test
	public void testGetUrlDomainNameWithWWW() {
		String url = "http://www.delfi.ee/more/andmore.jsp";
		assertEquals(Crawler.getUrlDomainName(url), "delfi.ee");
	}

	@Test
	public void testGetUrlPath() {
		String url = "http://127.0.0.1/more/andmore.jsp";
		assertEquals(Crawler.getUrlPath(url), "/more/andmore.jsp");
	}
	
	@Test
	public void testGetUrlPathNoPath() {
		String url = "http://127.0.0.1/";
		assertEquals(Crawler.getUrlPath(url), "");
	}
	
	@Test
	public void addLinkFromCacheToDBTest() throws Exception {
		class WriterMock implements IWriter {
			@Override
			public void close() throws Exception {
				// TODO Auto-generated method stub
				
			}
			@Override
			public boolean insertURLDataToDB(String url, int shareCount,
					List<WordCount> words) {
				// TODO Auto-generated method stub
				return true;
			}
		}
		String url = "http://127.0.0.1/";
		Crawler c = new Crawler();
		WriterMock db = new WriterMock();
		c.urlShares = 5;
		c.addLinkFromCacheToDB(db, url);
		assertEquals(c.toBeCrawledUrls.get(0), "http://127.0.0.1/");
	}

}
