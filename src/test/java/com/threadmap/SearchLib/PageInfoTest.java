/**
 * 
 */
package com.threadmap.SearchLib;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.bigtesting.fixd.Method;
import org.bigtesting.fixd.ServerFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.threadmap.SearchLib.wordcount.WordCount;

/**
 * @author karmo
 */
public class PageInfoTest {
	
	public static int PORT = 8089;
	ServerFixture server;
	
	@Before
	public void beforeEachTest() throws Exception {
	  server = new ServerFixture(PORT);
	  server.start();
	}
	
	@After 
	public void deleteOutputFile() {
        try {
			server.stop();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.PageInfo#countWords(int)}.
	 */
	@Test
	public void testCountWords() {
		String response = "<p1>    text <br> &amp \n text </p1> text";
		String page = "/test";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		PageInfo pageInfo = new PageInfo("http://localhost:" + PORT + page, null);
		try {
			List<WordCount> list = pageInfo.countWords(10);
			assertEquals(1, list.size());
			assertEquals(3, list.get(0).count);
			assertEquals("text", list.get(0).word);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.PageInfo#countWords(int)}.
	 */
	@Test
	public void testCountWordsTooMany() {
		String response = "aaa bbb ccc ddd fff ggg hhh iii";
		String page = "/test";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		PageInfo pageInfo = new PageInfo("http://localhost:" + PORT + page, null);
		try {
			List<WordCount> list = pageInfo.countWords(2);
			assertEquals(2, list.size());
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testSocialInfo() throws IOException {
		int expected = 15;
		PageInfo pageInfo = new PageInfo("", new APIRequestStub(expected));
		int actual = pageInfo.getSocialInfo().getShareCount();
		assertEquals(expected, actual);
	}

}
