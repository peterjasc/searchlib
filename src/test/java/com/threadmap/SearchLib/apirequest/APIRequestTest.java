/**
 * 
 */
package com.threadmap.SearchLib.apirequest;

import static org.junit.Assert.*;

import java.io.IOException;

import org.bigtesting.fixd.Method;
import org.bigtesting.fixd.ServerFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author karmo
 */
public class APIRequestTest {
	
	public static int PORT = 8092;
	ServerFixture server;
	
	@Before
	public void beforeEachTest() throws Exception {
	  server = new ServerFixture(PORT);
	  server.start();
	}
	
	@After
	public void afterEachTest() throws Exception {
		server.stop();
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.apirequest.APIRequest#getResultObject(java.lang.String)}.
	 * @throws IOException 
	 */
	@Test
	public void testGetResultObject() throws IOException {
		int expected = 15;
		String response = "{\"share\": {\"share_count\": " + expected+ "},\"id\":"
				+ " \"http://www.postimes.ee.ee\"}";
		String page = "/testingFB";
		
		server.handle(Method.GET, page)
	      .with(200, "application/json", response);
		
		APIRequest request = new APIRequest("http://127.0.0.1:" + PORT + page);
		int actual = request.getResultObject("").getShares();
		assertEquals(expected, actual);
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.apirequest.APIRequest#getResultObject(java.lang.String)}.
	 * @throws IOException 
	 */
	@Test
	public void testInvalidResponse() throws IOException {
		Integer expected = null;
		String response = "{\"share\": {\"share_count\": " + expected+ "},\"id\":"
				+ " \"http://www.postimes.ee.ee\"}";
		String page = "/testingFB";
		
		server.handle(Method.GET, page)
	      .with(200, "application/json", response);
		
		APIRequest request = new APIRequest("http://127.0.0.1:" + PORT + page);
		Integer actual = request.getResultObject("").getShares();
		assertEquals(expected, actual);
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.apirequest.APIRequest#getResultObject(java.lang.String)}.
	 * @throws IOException 
	 */
	@Test
	public void testNotFound() throws IOException {
		Integer expected = null;
		String response = "Page not found";
		String page = "/testingFB";
		
		server.handle(Method.GET, page)
	      .with(404, "application/json", response);
		
		APIRequest request = new APIRequest("http://127.0.0.1:" + PORT + page);
		APIRequestResultObject actual = request.getResultObject("");
		assertEquals(expected, actual);
	}

}
