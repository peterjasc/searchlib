/**
 * 
 */
package com.threadmap.SearchLib.database;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.threadmap.SearchLib.wordcount.WordCount;

/**
 * @author karmo
 */
public class DBWriterTest {
	
	private Database db;
	private DBWriter writer;

	private static final String DB_NAME = ":memory:";

	@Before
	public void setUp() throws Exception {
		db = new Database(DB_NAME, true);
		writer = new DBWriter(db);
	}

	@After
	public void tearDown() throws Exception {
		writer.close();
	}
	
	@Test
	public void testInsertURLDataToDB() {
		List<WordCount> words = new ArrayList<WordCount>();
		words.add(new WordCount("red", 3));
		//gives true if url doesnt exist and false if does.
		assertEquals(true, writer.insertURLDataToDB("urlTest5", 3, words));
		assertEquals(true, writer.insertURLDataToDB("urlTest6", 3, words));
		assertEquals(false, writer.insertURLDataToDB("urlTest5", 3, words));
	}

	@Test
	public void testAddLinkIfNotExistsInDB() throws SQLException {
		List<WordCount> words = new ArrayList<WordCount>();
		words.add(new WordCount("kitten", 3));
		writer.words = words;

		assertLinkInserted(true, "urlTest10");
		assertLinkInserted(false, "urlTest10");
	}

	private void assertLinkInserted(boolean expected, String url) throws SQLException {
		writer.url = url;
		writer.shareCount = 3;
		assertEquals(expected, writer.addLinkIfNotExistsInDB());
	}
	
	@Test
	public void testInsertIntoPageWordTable() throws SQLException {
		String word = "word";
		int count = 3;
		int pageID = insertToPageTable(5, "url");
		int wordID = writer.insertWordIntoDB(word);
		writer.insertIntoPageWordTable(wordID, pageID, count);
		
		ResultSet results = readPageWordTable(pageID, wordID);
		assertEquals(count, results.getInt("count"));
		assertEquals(pageID, results.getInt("Page_id"));
		assertEquals(wordID, results.getInt("Word_id"));
	}

	private int insertToPageTable(int shares, String url) throws SQLException {
		writer.url = url;
		writer.shareCount = shares;
		return writer.insertToPageTable();
	}

	private ResultSet readPageWordTable(int pageID, int wordID)
			throws SQLException {
		return db.getStatement().executeQuery(
				"SELECT * FROM Page_Word WHERE Word_id = " + wordID 
				+ " AND Page_id = " + pageID + ";");
	}
	
	@Test
	public void testInsertWords() throws SQLException {
		List<WordCount> words = Arrays.asList(new WordCount[] {
			new WordCount("aaaa", 3), new WordCount("bbbb", 5)
		});
		List<Integer> wordIDs = insertWordsGetIDs(words);
		assertCorrectIDWasReturned(words, wordIDs, 0);
		assertCorrectIDWasReturned(words, wordIDs, 1);
	}

	private void assertCorrectIDWasReturned(List<WordCount> list,
			List<Integer> wordIDs, int index) throws SQLException {
		Integer expected = writer.readWordID(list.get(index).getWord());
		assertEquals(expected, wordIDs.get(index));
	}
	
	@Test
	public void testConnectWords() throws SQLException {
		List<WordCount> words = Arrays.asList(new WordCount[] {
				new WordCount("aaaa", 3), new WordCount("bbbb", 5), new WordCount("cccc", 6)
			});
		List<Integer> wordIDs = insertWordsGetIDs(words);
		writer.connectWords();
		
		int noOfinserts = 1;
		assertAllWordConnections(words, wordIDs, noOfinserts);
	}
	
	private void assertAllWordConnections(List<WordCount> words,
			List<Integer> wordIDs, int noOfinserts) throws SQLException {
		for (int i = 0; i < wordIDs.size(); i++)
			for (int j = 0; j < wordIDs.size(); j++)
				if (i != j) {
					int expectedCount = words.get(i).getCount() 
							* words.get(j).getCount() * noOfinserts;
					assertWordsAreConnected(wordIDs, expectedCount, i, j);
				}
	}

	private void assertWordsAreConnected(List<Integer> wordIDs, 
			int expectedCount, int i, int j) throws SQLException {
		ResultSet results = readConnectedWordsTable(wordIDs.get(i), wordIDs.get(j));
		if (results.next()) {
			assertEquals(expectedCount, results.getInt("count"));
			assertEquals((int) wordIDs.get(j), results.getInt("connection"));
		}
	}
	
	private ResultSet readConnectedWordsTable(int wordID, int connection)
			throws SQLException {
		return db.getStatement().executeQuery(
				"SELECT connection, count FROM Connected_Words"
				+ " WHERE Word_id = " + wordID 
				+ " AND connection = " + connection + ";");
	}
	
	@Test
	public void testConnectWordsIncreaseCount() throws SQLException {
		List<WordCount> words = Arrays.asList(new WordCount[] {
				new WordCount("aaaa", 3), new WordCount("bbbb", 5)
			});
		List<Integer> wordIDs = insertWordsGetIDs(words);
		writer.connectWords();
		writer.connectWords();
		
		int noOfinserts = 2;
		assertAllWordConnections(words, wordIDs, noOfinserts);
	}

	private List<Integer> insertWordsGetIDs(List<WordCount> words)
			throws SQLException {
		writer.words = words;
		List<Integer> wordIDs = writer.insertWords();
		writer.wordIDs = wordIDs;
		return wordIDs;
	}

}
