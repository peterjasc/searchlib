/**
 * 
 */
package com.threadmap.SearchLib.database;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author karmo
 */
public class DatabaseFactoryTest {
	
	@Test
	public void testDatabaseFactory() {
		new DatabaseFactory();
	}

	@Test
	public void testCreateSearcher() throws Exception {
		assertNotEquals(null, DatabaseFactory.createSearcher(100));
	}
	
	@Test
	public void testCreateWriter() throws Exception {
		assertNotEquals(null, DatabaseFactory.createWriter());
	}

}
