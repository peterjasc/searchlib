package com.threadmap.SearchLib.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.threadmap.SearchLib.database.Database;

/**
 * 
 * @author Tommi
 *
 */
public class DatabaseTest {
	
	private static final String DB_NAME = ":memory:";
	private Database dbTest;
	@Before
	public void setUp() throws Exception {
		dbTest = new Database(DB_NAME, true);
	}

	@After
	public void tearDown() throws Exception {
		dbTest.closeConnection();
	}
	
	@Test
	public void testCheckIfWordExists() throws SQLException {
		String word = "karupoeg";
		dbTest.getStatement().executeUpdate(
				"INSERT INTO Word (word) VALUES ('" + word + "');");
		assertTrue(dbTest.WordExistsInDB(word));
		assertFalse(dbTest.WordExistsInDB("zzz"));
	}
	
	
	@Test
	public void testCloseConnection() throws Exception {
		String name = "testCloseConnection.db";
		try {
			Database db = new Database(name, false);
			db.closeConnection();
			assertEquals(true, db.c.isClosed());
		} finally {
			new File(name).delete();
		}
	}
}
