/**
 * 
 */
package com.threadmap.SearchLib.database;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.threadmap.SearchLib.wordcount.WordCount;

/**
 * @author karmo
 */
public class SearchResultsTest {
	
	private Database db;
	private SearchResults search;
	private DBWriter writer;
	private Statement stmt;

	private static final String DB_NAME = ":memory:";
	private static final int RESULT_SIZE = 5;

	@Before
	public void setUp() throws Exception {
		db = new Database(DB_NAME, true);
		search = new SearchResults(db, RESULT_SIZE);
		writer = new DBWriter(db);
		stmt = db.getStatement();
	}

	@After
	public void tearDown() throws Exception {
		search.close();
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.database.SearchResults#search(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testSearchNotFound() throws SQLException {
		Results results = search.search("abab");
		assertResultSize(0, results);
	}
	
	private void assertResultSize(int expected, Results results) {
		assertEquals(expected, results.counts.size());
		assertEquals(results.counts.size(), results.shares.size());
		assertEquals(results.counts.size(), results.urls.size());
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.database.SearchResults#search(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testSearch() throws SQLException {
		String url = "url";
		int shareCount = 3;
		WordCount wc = new WordCount("word", 11);
		List<WordCount> words = Arrays.asList(new WordCount[] { wc });
		writer.insertURLDataToDB(url, shareCount, words);
		
		Results results = search.search(wc.getWord());
		assertResultSize(1, results);
		
		assertEquals(wc.getCount(), (int) results.counts.get(0));
		assertEquals(shareCount, (int) results.shares.get(0));
		assertEquals(url, results.urls.get(0));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.database.SearchResults#search(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testSearchMultiWord() throws SQLException {
		String url = "url";
		int shareCount = 3;
		List<WordCount> words = Arrays.asList(new WordCount[] { 
				new WordCount("aaaa", 7), new WordCount("bbbb", 11)
			});
		writer.insertURLDataToDB(url, shareCount, words);
		
		Results results = search.search("aaaa bbbb");
		assertResultSize(1, results);
		
		assertEquals(18, (int) results.counts.get(0));
		assertEquals(shareCount, (int) results.shares.get(0));
		assertEquals(url, results.urls.get(0));
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.database.SearchResults#findConnected(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testFindConnected() throws SQLException {
		String[] words = new String[] {"aaaa", "bbbb", "cccc"};
		insertConnectedWords(words);
		
		List<WordCount> results = search.findConnected(words[0]);
		assertEquals(new WordCount(words[1], 3),  results.get(0));
		assertEquals(new WordCount(words[2], 2),  results.get(1));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.database.SearchResults#findConnected(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testFindConnectedMultiWord() throws SQLException {
		String[] words = new String[] {"aaaa", "bbbb", "cccc"};
		insertConnectedWords(words);
		
		List<WordCount> results = search.findConnected(words[0] + " " + words[1]);
		assertEquals(new WordCount(words[2], 9),  results.get(0));
	}

	private void insertConnectedWords(String[] words) throws SQLException {
		List<Integer> wordIDs = new ArrayList<>();
		for (int i = 0; i < words.length; i++) {
			wordIDs.add(writer.insertWordIntoDB(words[i]));
		}
		
		stmt.executeUpdate("INSERT INTO Connected_Words VALUES"
				+ "(" + wordIDs.get(0) + ", " + wordIDs.get(1) + ", 3);");
		stmt.executeUpdate("INSERT INTO Connected_Words VALUES"
				+ "(" + wordIDs.get(0) + ", " + wordIDs.get(2) + ", 2);");
		stmt.executeUpdate("INSERT INTO Connected_Words VALUES"
				+ "(" + wordIDs.get(1) + ", " + wordIDs.get(2) + ", 7);");
	}

	@Test
	public void testFindMostPopularOnePage() throws SQLException {
		WordCount wc = new WordCount("word", 11), wc2 = new WordCount("bbbb", 13);
		List<WordCount> words = Arrays.asList(new WordCount[] { wc, wc2 });
		writer.insertURLDataToDB("url", 1, words);
		
		List<WordCount> actual = search.findMostPopular();
		assertEquals(wc2, actual.get(0));
		assertEquals(wc, actual.get(1));
	}
	
	@Test
	public void testFindMostPopularThreePages() throws SQLException {
		WordCount wc = new WordCount("word", 11), wc2 = new WordCount("bbbb", 13);
		List<WordCount> words = Arrays.asList(new WordCount[] { wc, wc2 });
		writer.insertURLDataToDB("url", 1, words);
		writer.insertURLDataToDB("url2", 1, words);
		writer.insertURLDataToDB("url3", 1, words.subList(1, 2));
		
		List<WordCount> expected = Arrays.asList(new WordCount[] {
				new WordCount(wc2.getWord(), wc2.count * 3),
				new WordCount(wc.getWord(), wc.count * 2)
		});
		List<WordCount> actual = search.findMostPopular();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testFindMostPopularLimitResultSize() throws Exception {
		for (int i = 0; i < RESULT_SIZE + 5; i++)
			writeToPageWord(i);
		
		List<WordCount> actual = search.findMostPopular();
		assertEquals(RESULT_SIZE, actual.size());
	}

	private void writeToPageWord(int i) throws SQLException {
		writer.insertURLDataToDB("url" + i, 1, 
				Arrays.asList(new WordCount[] {
						new WordCount("word" + i, 11) 
				}));
	}
	
}
