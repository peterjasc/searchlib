/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import static org.junit.Assert.*;

import java.io.IOException;

import org.bigtesting.fixd.Method;
import org.bigtesting.fixd.ServerFixture;
import org.junit.Before;
import org.junit.Test;


/**
 * @author karmo
 *
 */
public class HtmlFilterTest {
	
	public static int PORT = 8090;
	ServerFixture server;
	
	@Before
	public void beforeEachTest() throws Exception {
	  server = new ServerFixture(PORT);
	  server.start();
	}

	@Test
	public void testHtml2text() {
		String response = "<p1>    text <br> &amp \n text </p1> text";
		String expected = "text & text text";
		String page = "/test";
		
		server.handle(Method.GET, page)
	      .with(200, "text/html", response);
		
		try {
			String actual = HtmlFilter.html2text(
					"http://localhost:" + PORT + page);
			assertEquals(expected, actual);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
