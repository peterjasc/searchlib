/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author karmo
 */
public class WordCountTest {
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.WordCount#WordCount(String, int)()}.
	 */
	@Test
	public void testCreate() {
		String word = "aaaa";
		int count = 5;
		WordCount wc = new WordCount(word, count);
		assertEquals(word, wc.getWord());
		assertEquals(count, wc.getCount());
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		String word = "aaaa";
		int count = 5;
		WordCount wc = new WordCount();
		wc.word = word;
		wc.count = count;
		WordCount wc2 = new WordCount(word, count);
		assertEquals(wc.hashCode(), wc2.hashCode());
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#hashCode()}.
	 */
	@Test
	public void testHashCodeNullWord() {
		int count = 5;
		assertEquals(new WordCount(null, count).hashCode(), 
				new WordCount(null, count).hashCode());
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		String word = "aaaa";
		int count = 5;
		WordCount wc = new WordCount();
		wc.word = word;
		wc.count = count;
		WordCount wc2 = new WordCount(word, count);
		assertEquals(wc, wc2);
		
		assertEquals(new WordCount(null, count), new WordCount(null, count));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObjectNullWord() {
		int count = 5;
		assertEquals(new WordCount(null, count), new WordCount(null, count));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsNull() {
		WordCount wc = new WordCount("a", 1);
		assertNotEquals(wc, null);;
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSelf() {
		WordCount wc = new WordCount("a", 1);
		assertEquals(wc, wc);
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsDifferentCount() {
		String word = "a";
		WordCount wc1 = new WordCount(word, 1);
		WordCount wc2 = new WordCount(word, 2);
		assertNotEquals(wc1, wc2);
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsDifferentWord() {
		int count = 5;
		assertNotEquals(new WordCount(null, count), new WordCount("", count));
		assertNotEquals(new WordCount("aa", count), new WordCount("bb", count));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordCount#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsDifferentClass() {
		assertNotEquals(new WordCount(null, 1), new Object());
	}

}
