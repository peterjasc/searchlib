/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.threadmap.SearchLib.wordcount.WordCounter;
import com.threadmap.SearchLib.wordcount.WordCounter.Counter;

/**
 * @author karmo
 *
 */
public class WordCounterTest {
	
	@Test
	public void testEmptyFindPopular() {
		assertEquals(0, new WordCounter().findPopular(null).size());
		assertEquals(0, new WordCounter().findPopular("").size());
	}

	@Test
	public void testFindPopular() {
		String line = "bbb ccc ccc";
		List<WordCount> list = new WordCounter().findPopular(line);
		assertEquals(2, list.size());
		assertEquals(2, list.get(0).getCount());
		assertEquals("ccc", list.get(0).getWord());
		assertEquals(1, list.get(1).getCount());
		assertEquals("bbb", list.get(1).getWord());
	}
	
	
	@Test
	public void testCountEmtyString() {
		assertEquals(0, new WordCounter().count(null).size());
		assertEquals(0, new WordCounter().count("").size());
	}
	
	@Test
	public void testCountOneWord() {
		String word = "word";
		Map<String, Counter> counts = new WordCounter().count(word);
		assertEquals(1, counts.size());
		assertEquals(1, counts.get(word).intValue());
	}
	
	@Test
	public void testCountThreeSameWords() {
		String line = "word \n word word";
		Map<String, Counter> counts = new WordCounter().count(line);
		assertEquals(1, counts.size());
		assertEquals(3, counts.get("word").intValue());
	}
	
	@Test
	public void testCountDifferentWords() {
		String line = "aaa bbb aaa";
		Map<String, Counter> counts = new WordCounter().count(line);
		assertEquals(2, counts.size());
		assertEquals(2, counts.get("aaa").intValue());
		assertEquals(1, counts.get("bbb").intValue());
	}
	
	@Test
	public void testMapToList() {
		String line = "aaa bb1b cc-c a2aa";
	    Map<String, Counter> counts = new WordCounter().count(line);
	    assertEquals(3, counts.size());
	    
	    List<WordCount> wordCounts = new WordCounter().mapToList(counts);
	    assertEquals(3, wordCounts.size());
	    
	    assertListContains(wordCounts, 2, "aaa");
	    assertListContains(wordCounts, 1, "ccc");
	    assertListContains(wordCounts, 1, "bbb");
	}

	private void assertListContains(List<WordCount> wordCounts, int expectedSize,
			String expectedWord) {
		for (WordCount wc : wordCounts) {
			if (wc.word.equals(expectedWord)) {
				assertEquals(expectedSize, wc.count);
				return;
			}
		}
		fail(expectedWord + " not found in the list of word counts.");
	}

}
