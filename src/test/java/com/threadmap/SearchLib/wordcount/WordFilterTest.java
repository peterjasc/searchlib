/**
 * 
 */
package com.threadmap.SearchLib.wordcount;

import static org.junit.Assert.*;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author karmo
 */
public class WordFilterTest {
	
	private static final String EXCULION_FILE = "exclude_test.txt";
	private WordFilter filter;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try (PrintWriter out = new PrintWriter(EXCULION_FILE)) {
			out.println("aaa");
			out.println("bbb");
			out.println("ccc");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		new File(EXCULION_FILE).delete();
	}
	
	@Before
	public void setUp() throws Exception {
		filter = new WordFilter(EXCULION_FILE);
	}

	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordFilter#filter(java.util.List)}.
	 */
	@Test
	public void testFilterNothing() {
		List<WordCount> before = new ArrayList<>();
		List<WordCount> actual = filter.filter(before);
		assertTrue(actual.equals(new ArrayList<>()));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordFilter#filter(java.util.List)}.
	 */
	@Test
	public void testFilterEverything() {
		List<WordCount> before = Arrays.asList(new WordCount[] {
				new WordCount("aaa", 3),
				new WordCount("bbb", 3),
				new WordCount("ccc", 3)
		});
		List<WordCount> actual = filter.filter(before);
		assertTrue(actual.equals(new ArrayList<>()));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordFilter#filter(java.util.List)}.
	 */
	@Test
	public void testFilterCaseInsensitive() {
		List<WordCount> before = Arrays.asList(new WordCount[] {
				new WordCount("aAa", 3),
				new WordCount("BBB", 3),
				new WordCount("ccc", 3)
		});
		List<WordCount> actual = filter.filter(before);
		assertTrue(actual.equals(new ArrayList<>()));
	}
	
	/**
	 * Test method for {@link com.threadmap.SearchLib.wordcount.WordFilter#filter(java.util.List)}.
	 */
	@Test
	public void testFilterSome() {
		List<WordCount> before = Arrays.asList(new WordCount[] {
				new WordCount("a", 3),
				new WordCount("aaa", 3),
				new WordCount("c", 3)
		});
		List<WordCount> expected = Arrays.asList(new WordCount[] {
				new WordCount("a", 3),
				new WordCount("c", 3)
		});
		
		List<WordCount> actual = filter.filter(before);
		assertTrue(actual.equals(expected));
	}

}
